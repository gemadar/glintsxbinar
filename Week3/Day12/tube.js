const blabla = require("./blabla");

class Tube extends blabla {
  constructor(innerRadius, height, outerRadius) {
    super("Tube");
    this.innerRadius = innerRadius;
    this.height = height;
    this.outerRadius = outerRadius;
  }

  calculateCircumference() {
    super.calculateCircumference();
    return `${
      2 * Math.PI * this.innerRadius + 2 * Math.PI * this.outerRadius
    } cm2`;
  }
  calculateVolume() {
    super.calculateVolume();
    return `${
      Math.PI * (this.outerRadius ** 2 - this.innerRadius ** 2) * this.height
    } cm3`;
  }
}

module.exports = Tube;
