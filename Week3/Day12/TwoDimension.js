const Geometry = require("./geometry");

class TwoDimension extends Geometry {
	constructor(name) {
		super(name, "Two Dimension");

		if (this.constructor === TwoDimension) {
			throw new Error("Two Dimension is a abstract class");
		}
	}

	Type() {
		super.Type();
		console.log(`I am ${this.type}`);
	}

	calculateCircumference() {
		console.log(`\n${this.name} Circumference!\n=====================`);
	}

	calculateArea() {
		console.log(`\n${this.name} Area!\n==============================`);
	}
}

module.exports = TwoDimension;
