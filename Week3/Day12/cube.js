const ThreeDimension = require("./threeDimension");

class cube extends ThreeDimension {
	constructor(edge) {
		super("cube");
		this.edge = edge;
	}

	whoAmI() {
		super.Type();
		console.log(`I'm ${this.name}`);
	}

	calculateVolume(whoAreYou) {
		const who = `${whoAreYou} is trying to calculate Cube Volume: `;
		return `${who} ${this.edge * this.edge * this.edge} cm3`;
	}

	calculateSurface(whoAreYou) {
		const who = `${whoAreYou} is trying to calculate Cube Surface: `;
		let r2 = this.radius ** 2;
		return `${who} ${6 * this.edge ** 2} cm2`;
	}
}

module.exports = cube;
