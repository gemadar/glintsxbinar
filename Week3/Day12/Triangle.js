const TwoDimension = require("./TwoDimension");

class Triangle extends TwoDimension {
	constructor(a, b, c) {
		super("Triangle");
		this.a = a;
		this.b = b;
		this.c = c;
	}

	// Overridding
	whatWeAre() {
		this.Type();
	}

	Type() {
		super.Type();
		console.log(`I am ${this.name}`);
	}

	// Overloading
	calculateCircumference(whoAreYou) {
		super.calculateCircumference();
		const yourName = `${whoAreYou} is trying to calculate circumference:`;
		console.log(`${yourName} ${this.a + this.b + this.c} cm`);
		return this.a + this.b + this.c;
	}

	calculateArea(whoAreYou) {
		super.calculateArea();
		const yourName = `${whoAreYou} is trying to calculate area:`;
		let s = (this.a + this.b + this.c) / 2;
		let area = Math.sqrt(s * (s - this.a) * (s - this.b) * (s - this.c));
		console.log(`${yourName} ${area} cm2`);
		return area;
	}
}

module.exports = Triangle;
