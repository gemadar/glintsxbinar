const Beam = require("./Day 13/beam");

const beam1 = new Beam(12, 7, 5);
beam1.Type();
console.log(beam1.calculateArea("Bayu"));
console.log(beam1.calculateCircumference("Bayu"));

const Triangle = require("./Day 13/Triangle");

const triangle1 = new Triangle(12, 10, 12);
triangle1.Type();
console.log(triangle1.calculateCircumference("Kimbraien"));
console.log(triangle1.calculateArea("Kimbraien"));

const Rectangle = require("./Day 13/Rectangle");

const rectangle1 = new Rectangle(12, 5);
rectangle1.Type();
console.log(rectangle1.calculateCircumference("Kimbraien"));
console.log(rectangle1.calculateArea("Kimbraien"));

const Cone = require("./Day 13/cone");

const cone1 = new Cone(12, 5);
// cone1.Type();
console.log(cone1.calculateCircumference("Gema"));
console.log(cone1.calculateVolume("Gema"));

const Tube = require("./Day 13/tube");

const tube1 = new Tube(12, 5, 10);
// cone1.Type();
console.log(tube1.calculateCircumference("Gema"));
console.log(tube1.calculateVolume("Gema"));
