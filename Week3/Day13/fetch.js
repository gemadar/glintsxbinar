const fetch = require("node-fetch");

const url1 = "https://jsonplaceholder.typicode.com/posts";
const url2 = "https://jsonplaceholder.typicode.com/posts/1";
const url3 = "https://jsonplaceholder.typicode.com/posts/1/comments";
const url4 = "https://jsonplaceholder.typicode.com/comments?postId=1";

// Promise
fetch("https://jsonplaceholder.typicode.com/posts")
  .then((res) => res.json())
  .then((data) => console.log(data));

fetch("https://jsonplaceholder.typicode.com/posts/1")
  .then((res) => res.json())
  .then((data) => console.log(data));

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
  .then((res) => res.json())
  .then((data) => console.log(data));

fetch("https://jsonplaceholder.typicode.com/comments?postId=1")
  .then((res) => res.json())
  .then((data) => console.log(data));

// Async await
async function fetchAPI() {
  const response = await fetch(url1);
  const data = await response.json();
  console.log(data);

  response = await fetch(url2);
  data = await response.json();
  console.log(data);

  response = await fetch(url3);
  data = await response.json();
  console.log(data);

  response = await fetch(url4);
  data = await response.json();
  console.log(data);
}
fetchAPI();
