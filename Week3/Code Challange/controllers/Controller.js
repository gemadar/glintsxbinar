const harryPotter = require("../models/harryPotter.json");

class Controller {
  getAllharryPotter(req, res) {
    try {
      res.status(200).json({
        data: harryPotter,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  getOneharryP(req, res) {
    try {
      const harryP = harryPotter.filter(
        (harryP) => harryP.title === eval(req.params.title)
      );

      res.status(200).json({
        data: harryP,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  createHarryP(req, res) {
    try {
      harryPotter.push(req.body);

      res.status(201).json({
        data: harryPotter,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  putHarryP(req, res) {
    try {
      let harryP = harryPotter.filter(
        (harryP) => harryP.title === eval(req.params.title)
      );
      harryP[0].title = req.body.title;
      harryP[0].year = req.body.year;

      res.status(201).json({
        data: harryPotter,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  deleteHarryP(req, res) {
    try {
      let delHP = harryPotter.filter((harryP) => {
        return harryP.title !== eval(req.params.title);
      });

      res.status(201).json({
        data: delHP,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
}

module.exports = new Controller();
