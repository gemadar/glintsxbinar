const express = require("express"); // Import express

const {
  getAllharryPotter,
  getOneharryP,
  createHarryP,
  putHarryP,
  deleteHarryP,
} = require("../controllers/Controller");

const router = express.Router(); // Make router to define the routes

/* If user go to (GET) http://localhost:3000/ or http://localhost:3000, it will go to here */
/* GET used to GET the data */
router.get("/", getAllharryPotter);

router.get("/:title", getOneharryP);

/* If user go to (POST) http://localhost:3000/ or http://localhost:3000, it will go to here */
/* POST used to send data from client */
router.post("/", createHarryP);

/* If user go to (PUT) http://localhost:3000/ or http://localhost:3000, it will go to here */
/* PUT used to change the data */
router.put("/:title", putHarryP);

/* If user go to (DELETE) http://localhost:3000/ or http://localhost:3000, it will go to here */
/* DELETE used to delete data */
router.delete("/:title", deleteHarryP);

module.exports = router; // export router
