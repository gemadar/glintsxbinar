const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function sphereVol(r) {
  console.log(`Your sphere volume is ${(4 / 3) * Math.PI * r ** 3}`);
}

function coneVol(r, h) {
  console.log(`Your cone volume is ${(1 / 3) * Math.PI * r ** 2 * h}`);
}

function octahedroneVol(a) {
  console.log(`Your octahedrone volume is ${(Math.sqrt(2) / 3) * a ** 3}`);
}

console.log(
  "This is geometry shape volume calculator which consist:\n Sphere\n Cone\n Octahedrone"
);
rl.question("What is the radius(r) of your sphere?", (r) => {
  sphereVol(r);
  rl.question("What is the radius(r) of your cone?", (r) => {
    rl.question("What is the height(h) of your cone?", (h) => {
      coneVol(r, h);
      rl.question("What is the radius(a) of your octahedrone?", (a) => {
        octahedroneVol(a);
        rl.close();
      });
    });
  });
});

rl.on("close", () => {
  process.exit();
});

/* Credits:
https://gitlab.com/binarxglints_batch13/
backendclass/boilerplate/-/blob/main/
Code%20Challenges/Week%201.md

https://www.splashlearn.com/
math-vocabulary/geometry/volume

https://secure.math.ubc.ca/
~cass/courses/m308/projects/cchang/
webpages/octahedron.html#:~:text=
The%20octahedron%20can%20be%20divided,%
C3%97%20the%20volume%20of%20pyramid. */
