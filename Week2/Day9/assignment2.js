const patient = [
  { name: "John", status: "positive" },
  { name: "Paul", status: "suspect" },
  { name: "Ringo", status: "negative" },
  { name: "George", status: "suspect" },
  { name: "Lennon", status: "positive" },
];

exports.patientStatus = function () {
  patient.forEach((patientData) => {
    switch (patientData.status) {
      case "positive":
        console.log(
          `${patientData.name} is currently ${patientData.status} of COVID-19.`
        );
        break;
      case "egative":
        console.log(
          `${patientData.name} is currently ${patientData.status} of COVID-19.`
        );
        break;
      case "suspect":
        console.log(
          `${patientData.name} is currently ${patientData.status} of COVID-19.`
        );
        break;
      default:
        console.log(`${patientData.name} status of COVID-19 is being checked.`);
        break;
    }
  });
};
// module.exports = { patientStatus };
