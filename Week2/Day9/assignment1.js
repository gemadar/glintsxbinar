let food = ["tomato", "broccoli", "kale", "cabbage", "apple"];

food.forEach((veggies) => {
  if (veggies == "tomato") {
    console.log(
      `${veggies} is a great source of vitamin C, potassium, folate, and vitamin K`
    );
  } else if (veggies == "broccoli") {
    console.log(`${veggies} is a good source of fibre and protein`);
  } else if (veggies == "kale") {
    console.log(
      `A cup of cooked ${veggies} provides almost five times an adult's daily need for vitamin K`
    );
  } else if (veggies == "cabbage") {
    console.log(
      `${veggies} can raise levels of beta-carotene, lutein, and other heart-protective antioxidants`
    );
  } else {
    console.log(`${veggies} is not a vegetable`);
  }
});
