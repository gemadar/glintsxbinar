const EventEmitter = require("events");
const readline = require("readline");
const { patientStatus } = require("../Day9/assignment2");

const now = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Login Listener
now.on("login:failed", (email) => {
  console.log(`${email} is failed to login`);
});

// Login success
now.on("login:success", (email) => {
  console.log(`${email} success login`);
});

function login(email, password) {
  const passwordReal = "4312";
  const usernameReal = "hospitalC-19";
  if (email != usernameReal && password != passwordReal) {
    now.emit("login:failed", email);
    rl.close();
  } else {
    now.emit("login:success", email);
    patientStatus();
    rl.close();
  }
}

rl.question("Email: ", (email) => {
  rl.question("Password: ", (password) => {
    login(email, password); // Run login function
  });
});
