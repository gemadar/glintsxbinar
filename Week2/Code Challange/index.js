const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  var data = clean(data);
  for (let a = 0; a < data.length; a++) {
    for (let b = 0; b < data.length - a - 1; b++) {
      if (data[b] > data[b + 1]) {
        let c = data[b];
        data[b] = data[b + 1];
        data[b + 1] = c;
      }
    }
  }
  return data;
}

// Should return array
function sortDecending(data) {
  // Code Here
  var data = clean(data);
  for (let a = 0; a < data.length; a++) {
    for (let b = 0; b < data.length - a - 1; b++) {
      if (data[b] < data[b + 1]) {
        let c = data[b];
        data[b] = data[b + 1];
        data[b + 1] = c;
      }
    }
  }
  return data;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
