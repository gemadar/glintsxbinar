const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function sphereVol(radius) {
  return (4 / 3) * Math.PI * radius ** 3;
}

function coneVol(rad, height) {
  return (1 / 3) * Math.PI * rad ** 2 * height;
}

function octahedroneVol(edge) {
  return (Math.sqrt(2) / 3) * edge ** 3;
}

function input() {
  rl.question("Radius: ", function (radius) {
    console.log(radius);
    if (!isNaN(radius)) {
      console.log(`\nSphere: ${sphereVol(radius)}\n`);
      console.log(`Cone`);
      console.log(`=========`);
      input1();
    } else {
      console.log(`Radius, must be a number\n`);
      input();
    }
  });
  function input1() {
    rl.question("Rad: ", function (rad) {
      console.log(rad);
      rl.question("Height: ", (height) => {
        if (!isNaN(rad) && !isNaN(height)) {
          console.log(`\nCone: ${coneVol(rad, height)}\n`);
          console.log(`Octahedrone`);
          console.log(`=========`);
          input2();
        } else {
          console.log(`Rad and Height must be a number\n`);
          input1();
        }
      });
    });
  }
  function input2() {
    rl.question("Edge: ", function (edge) {
      console.log(edge);
      if (!isNaN(edge)) {
        console.log(`\nOctahedrone: ${octahedroneVol(edge)}`);
        rl.close();
      } else {
        console.log(`Edge, must be a number\n`);
        input2();
      }
    });
  }
}

console.log(`Sphere`);
console.log(`=========`);
input();
